# [Places](https://gitlab.com/drewhamilton/places-android)
An app that shows you a list of places, and links you to a map if you click on that place.

Designed with an MVP UI on top of a layered architecture with as few third-party libraries as
possible. The only libraries used are Android compatibility libraries (`AppCompat`, `RecyclerView`,
and `CardView`), Gson, and testing libraries.

## Some notes:
* The JSON file backing this app was original called `cities.json`, but I used the word "places"
  instead of "cities" because some of the places in the list are not cities.
* The list contains over 200,000 places, so performance is a consideration in
  a few contexts that it normally wouldn't be, such as converting data models to UI models.
* Initial startup is slow; I focused on making searching performant over initial startup.
* I've never done an app without Dagger; doing so was an interesting experience. This app's
  dependency injection framework consists of a single class called `Provider`, which leaves a lot
  to be desired in terms of constructor visibility of important app classes.
* Returning to threading without RxJava was a bit painful, especially with the goal of keeping
  Android dependencies out of the presenter layer. I made do by wrapping `AsyncTask` in an Rx-like
  class called `AsyncChain`.

## To-do
* Searching is implemented by filtering a list based on the filter string. This is plenty fast on
  modern devices, but noticeably slower on older devices. Consider using a prefix tree for faster
  filtering.
* Add some sort of error handling for devices that don't have enough memory to load the places list.
* Make a custom app icon using the app's color scheme.
* Review app architecture and tests to see if I missed anything or implemented anything in a strange
  manner.
* Improve startup time.
* Use libraries where they can improve the app's architecture significantly more easily than I can
  myself, especially Dagger and RxJava. 
