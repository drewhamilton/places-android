package drewhamilton.places.provide;

import android.content.Context;

import drewhamilton.places.async.AsyncChainProvider;
import drewhamilton.places.data.PlaceRepository;
import drewhamilton.places.ui.PresenterFactory;
import drewhamilton.places.ui.list.ListPresenter;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestProvider extends Provider {

    //region Presenter singletons
    private static final ListPresenter MOCK_LIST_PRESENTER = mock(ListPresenter.class);
    //endregion

    public TestProvider(Context context) {
        super(context);
    }

    @Override
    PresenterFactory newPresenterFactory() {
        PresenterFactory mockPresenterFactory = mock(PresenterFactory.class);
        when(mockPresenterFactory.listPresenter()).thenReturn(MOCK_LIST_PRESENTER);
        return mockPresenterFactory;
    }

    @Override
    PlaceRepository newPlaceRepository() {
        return mock(PlaceRepository.class);
    }

    @Override
    AsyncChainProvider newAsyncChainProvider() {
        return mock(AsyncChainProvider.class);
    }

    public ListPresenter getMockListPresenter() {
        return MOCK_LIST_PRESENTER;
    }
}
