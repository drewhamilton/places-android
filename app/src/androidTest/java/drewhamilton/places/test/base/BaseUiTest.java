package drewhamilton.places.test.base;

import android.content.Intent;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.app.AppCompatActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeUnit;

import drewhamilton.places.provide.Provider;
import drewhamilton.places.provide.TestProvider;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static drewhamilton.places.test.tools.AppViewActions.loopMainThread;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public abstract class BaseUiTest<A extends AppCompatActivity> {

    @Rule public IntentsTestRule<A> testRule = new IntentsTestRule<>(getActivityClass(), true, false);

    private TestProvider testProvider;

    @Before
    @CallSuper
    public void setUp() {
        testProvider = new TestProvider(InstrumentationRegistry.getTargetContext());
        Provider.overrideInstance(testProvider);
    }

    @NonNull
    protected abstract Class<A> getActivityClass();

    protected void launchActivity() {
        launchActivity(new Intent());
    }

    protected void launchActivity(Intent intent) {
        testRule.launchActivity(intent);
        onActivityLaunched();
    }

    protected void onActivityLaunched() {

    }

    @NonNull
    protected A getActivity() {
        final A activity = testRule.getActivity();
        if (activity == null) {
            fail("Called getActivity() before launching Activity");
            return null;
        } else {
            return activity;
        }
    }

    protected TestProvider getProvider() {
        return testProvider;
    }

    protected void runOnUiThread(Runnable runnable) {
        try {
            testRule.runOnUiThread(runnable);
        } catch (Throwable throwable) {
            fail("Error while running on UI thread: " + throwable.getMessage());
        }
    }

    protected void waitForUiThread() {
        InstrumentationRegistry.getInstrumentation().waitForIdleSync();
    }

    protected void waitForUiThread(int time, TimeUnit unit) {
        onView(isRoot()).perform(loopMainThread(time, unit));
    }

    protected void simulateConfigurationChange() {
        runOnUiThread(() -> getActivity().recreate());
        waitForUiThread();
    }
}
