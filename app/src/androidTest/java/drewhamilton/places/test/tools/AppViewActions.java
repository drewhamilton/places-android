package drewhamilton.places.test.tools;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.view.View;

import org.hamcrest.Matcher;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.any;

public class AppViewActions {

    private AppViewActions() {}

    public static ViewAction loopMainThread(final int time, final TimeUnit unit) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return any(View.class);
            }

            @Override
            public String getDescription() {
                return "loop main thread for " + toMilliseconds(time, unit) + " milliseconds";
            }

            @Override
            public void perform(UiController uiController, View view) {
                uiController.loopMainThreadForAtLeast(toMilliseconds(time, unit));
            }

            private long toMilliseconds(int time, TimeUnit unit) {
                return TimeUnit.MILLISECONDS.convert(time, unit);
            }
        };
    }
}
