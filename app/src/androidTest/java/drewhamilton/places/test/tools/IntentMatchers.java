package drewhamilton.places.test.tools;

import android.net.Uri;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.Locale;

public class IntentMatchers {

    public static Matcher<Uri> coordinateUri(double latitude, double longitude) {
        final String mapScheme = "geo";
        final String coordinateDataTemplate = "%f,%f";

        final String expectedSsp = String.format(Locale.getDefault(), coordinateDataTemplate, latitude, longitude);
        final Uri expectedUri = Uri.fromParts(mapScheme, expectedSsp, null);

        return new TypeSafeMatcher<Uri>() {
            @Override
            protected boolean matchesSafely(Uri item) {
                return item.equals(expectedUri);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("with Uri ").appendValue(expectedUri);
            }
        };
    }

    private IntentMatchers() {
        throw new UnsupportedOperationException();
    }
}
