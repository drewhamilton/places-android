package drewhamilton.places.ui;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import org.junit.Test;

import drewhamilton.places.R;
import drewhamilton.places.test.base.BaseUiTest;
import drewhamilton.places.ui.list.ListFragment;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;


public class AppActivityTest extends BaseUiTest<AppActivity> {

    @IdRes private static final int FRAGMENT_CONTAINER_ID = R.id.fragment_container;

    @NonNull
    @Override
    protected Class<AppActivity> getActivityClass() {
        return AppActivity.class;
    }

    @Test
    public void onCreate_attachesListFragment() {
        launchActivity();

        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(FRAGMENT_CONTAINER_ID);
        assertNotNull(fragment);
        assertTrue(fragment instanceof ListFragment);
    }

    @Test
    public void onConfigChange_doesNotReplaceFragment() {
        launchActivity();

        final ListFragment fragment = (ListFragment) getActivity().getSupportFragmentManager()
                .findFragmentById(FRAGMENT_CONTAINER_ID);

        simulateConfigurationChange();

        final ListFragment newFragment = (ListFragment) getActivity().getSupportFragmentManager()
                .findFragmentById(FRAGMENT_CONTAINER_ID);

        assertSame(fragment, newFragment);
    }
}
