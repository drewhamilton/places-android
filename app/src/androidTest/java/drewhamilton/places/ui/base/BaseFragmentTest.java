package drewhamilton.places.ui.base;

import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import org.junit.Test;

import drewhamilton.places.test.base.BaseUiTest;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;

public abstract class BaseFragmentTest
        <V, P extends Presenter<V>, F extends BaseFragment<V, P>, A extends AppCompatActivity>
        extends BaseUiTest<A> {

    @Test
    public void newPresenter_fromProvider() {
        launchActivity();
        assertEquals(getMockPresenter(), getFragment().newPresenter());
    }

    @Test
    public void onCreate_attachesToPresenter() {
        launchActivity();

        verify(getMockPresenter()).attachView((V) getFragment());
    }

    @Test
    public void onConfigurationChange_reattachesToPresenter() {
        launchActivity();

        simulateConfigurationChange();
        verify(getMockPresenter(), timeout(200)).reattachView((V) isA(getFragmentClass()));
    }

    @Test
    public void getLayout_returnsExpectedLayout() {
        launchActivity();
        assertEquals(getExpectedLayout(), getFragment().getLayout());
    }

    protected abstract Class<F> getFragmentClass();

    protected abstract F getFragment();

    protected abstract P getMockPresenter();

    @LayoutRes
    protected abstract int getExpectedLayout();
}
