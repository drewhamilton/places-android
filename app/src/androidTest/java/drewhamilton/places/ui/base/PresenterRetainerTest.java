package drewhamilton.places.ui.base;

import android.support.annotation.NonNull;

import org.junit.Test;

import drewhamilton.places.test.base.BaseUiTest;

import static org.junit.Assert.assertSame;

public class PresenterRetainerTest extends BaseUiTest<TestPresenterRetainerActivity> {

    @NonNull
    @Override
    protected Class<TestPresenterRetainerActivity> getActivityClass() {
        return TestPresenterRetainerActivity.class;
    }

    @Test
    public void configurationChange_savesData() {
        final Presenter presenter = new Presenter() {};

        launchActivity();
        runOnUiThread(() -> getActivity().setPresenter(presenter));
        assertSame(presenter, getActivity().getPresenter());

        simulateConfigurationChange();
        assertSame(presenter, getActivity().getPresenter());
    }
}
