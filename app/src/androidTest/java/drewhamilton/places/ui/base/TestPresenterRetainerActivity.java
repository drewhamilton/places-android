package drewhamilton.places.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public class TestPresenterRetainerActivity extends AppCompatActivity {

    private static final String TEST_TAG = "Test tag";

    private PresenterRetainer<Presenter> dataRetainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataRetainer = PresenterRetainer.initialize(getSupportFragmentManager(), TEST_TAG);
    }

    Presenter getPresenter() {
        return dataRetainer.getPresenter();
    }

    void setPresenter(Presenter presenter) {
        dataRetainer.setPresenter(presenter);
    }
}
