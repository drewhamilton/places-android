package drewhamilton.places.ui.list;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.test.espresso.matcher.ViewMatchers;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import drewhamilton.places.R;
import drewhamilton.places.ui.AppActivity;
import drewhamilton.places.ui.base.BaseFragmentTest;
import drewhamilton.places.ui.map.MappedPlaceModel;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static drewhamilton.places.test.tools.IntentMatchers.coordinateUri;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class ListFragmentTest extends BaseFragmentTest<ListView, ListPresenter, ListFragment, AppActivity> {

    private static final String TEST_PLACE_NAME = "Test place %d";

    @Override
    protected Class<ListFragment> getFragmentClass() {
        return ListFragment.class;
    }

    @Override
    protected ListFragment getFragment() {
        return (ListFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
    }

    @Override
    protected ListPresenter getMockPresenter() {
        return getProvider().getMockListPresenter();
    }

    @LayoutRes
    @Override
    protected int getExpectedLayout() {
        return R.layout.fragment_list;
    }

    @NonNull
    @Override
    protected Class<AppActivity> getActivityClass() {
        return AppActivity.class;
    }

    @Test
    public void displayPlacesInList_displaysAllPlaces() {
        final List<ListedPlaceModel> places = createListedPlaces(5);
        launchActivity();
        onView(withId(R.id.waiting)).check(matches(isDisplayed()));
        onView(withId(R.id.filter_container))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.INVISIBLE)));

        runOnUiThread(() -> getFragment().displayPlacesInList(places));

        onView(withId(R.id.waiting)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
        onView(withId(R.id.filter_container)).check(matches(isDisplayed()));
        for (ListedPlaceModel place : places) {
            onView(withText(place.fullName)).check(matches(isDisplayed()));
        }
    }

    @Test
    public void displayPlaceOnMap_launchesMapIntent() {
        final double testLatitude = 1.23456;
        final double testLongitude = 3.45678;
        final MappedPlaceModel testModel = new MappedPlaceModel(testLatitude, testLongitude);

        launchActivity();
        intending(hasAction(Intent.ACTION_VIEW))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK, new Intent()));

        runOnUiThread(() -> getFragment().displayPlaceOnMap(testModel));

        intended(hasAction(Intent.ACTION_VIEW));
        intended(hasData(coordinateUri(testLatitude, testLongitude)));
    }

    @Test
    public void clickItem_passesUiModelToPresenter() {
        final List<ListedPlaceModel> places = createListedPlaces(1);
        launchActivity();
        runOnUiThread(() -> getFragment().displayPlacesInList(places));

        onView(withText(getPlaceName(0)))
                .check(matches(isDisplayed()))
                .perform(click());

        verify(getMockPresenter()).showMap(places.get(0));
    }

    @Test
    public void enterFilter_passesFilterToPresenter() {
        final List<ListedPlaceModel> places = createListedPlaces(1);
        launchActivity();
        runOnUiThread(() -> getFragment().displayPlacesInList(places));
        reset(getMockPresenter());

        final String testFilter = "Test";
        onView(withId(R.id.filter))
                .check(matches(isDisplayed()))
                .check(matches(withHint(R.string.filter)))
                .perform(typeText(testFilter));

        for (int i = 1; i <= testFilter.length(); ++i) {
            final String subFilter = testFilter.substring(0, i);
            verify(getMockPresenter()).applyFilter(subFilter);
        }
        verifyNoMoreInteractions(getMockPresenter());

        onView(withId(R.id.filter))
                .check(matches(isDisplayed()))
                .perform(replaceText(""));
        verify(getMockPresenter()).applyFilter("");
        verifyNoMoreInteractions(getMockPresenter());
    }

    private static List<ListedPlaceModel> createListedPlaces(int count) {
        List<ListedPlaceModel> list = new ArrayList<>(count);
        for (int i = 0; i < count; ++i) {
            list.add(new ListedPlaceModel(i, getPlaceName(i)));
        }
        return list;
    }

    private static String getPlaceName(int position) {
        return String.format(Locale.US, TEST_PLACE_NAME, position);
    }
}
