package drewhamilton.places;

import android.app.Application;

import drewhamilton.places.provide.Provider;

public class PlacesApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Provider.initialize(this);
    }
}
