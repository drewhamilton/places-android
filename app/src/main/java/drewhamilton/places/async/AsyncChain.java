package drewhamilton.places.async;

import android.support.annotation.VisibleForTesting;

import java.util.concurrent.Callable;

/**
 * Wraps {@link android.os.AsyncTask} so data-layer classes don't have to depend on Android, and so
 * calling methods asynchronously is similar in syntax to RxJava
 *
 * Chained calls to the same method will always override any previous calls to that method.
 *
 * Complex threading is not supported.
 */
public class AsyncChain<T> {

    private ChainableAsyncTask<T> asyncTask;

    AsyncChain(ChainableAsyncTask<T> asyncTask) {
        this.asyncTask = asyncTask;
    }

    public AsyncChain<T> preExecuteOnUiThread(Runnable preExecute) {
        asyncTask.setPreExecuteRunnable(preExecute);
        return this;
    }

    AsyncChain<T> executeOnBackgroundThread(Callable<T> execute) {
        asyncTask.setDoInBackgroundCallable(execute);
        return this;
    }

    public AsyncChain<T> postExecuteOnUiThread(ResultRunnable<T> postExecute) {
        asyncTask.setPostExecuteRunnable(postExecute);
        return this;
    }

    public AsyncChain<T> executeOnCancelledOnUiThread(ResultRunnable<T> cancelled) {
        asyncTask.setCancelledRunnable(cancelled);
        return this;
    }

    public AsyncSubscription subscribe() {
        asyncTask.execute();
        return new AsyncSubscription(asyncTask);
    }

    @VisibleForTesting
    final ChainableAsyncTask<T> getAsyncTask() {
        return asyncTask;
    }
}
