package drewhamilton.places.async;

import java.util.concurrent.Callable;

public class AsyncChainProvider {

    public <T> AsyncChain<T> single(Callable<T> executeOnBackgroundThread) {
        return new AsyncChain<T>(new ChainableAsyncTask<>())
                .executeOnBackgroundThread(executeOnBackgroundThread);
    }
}
