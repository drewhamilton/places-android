package drewhamilton.places.async;

import android.support.annotation.NonNull;

public class AsyncSubscription {

    @NonNull private final ChainableAsyncTask asyncTask;

    AsyncSubscription(@NonNull ChainableAsyncTask asyncTask) {
        this.asyncTask = asyncTask;
    }

    public void cancel() {
        asyncTask.cancel(true);
    }
}
