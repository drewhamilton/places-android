package drewhamilton.places.async;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.concurrent.Callable;

class ChainableAsyncTask<T> extends AsyncTask<Void, Void, T> {

    @NonNull private Runnable preExecuteRunnable = new NoOpRunnable();
    @NonNull private Callable<T> doInBackgroundCallable = new NoOpCallable<>();
    @NonNull private ResultRunnable<T> postExecuteRunnable = new NoOpResultRunnable<>();
    @NonNull private ResultRunnable<T> cancelledRunnable = new NoOpResultRunnable<>();

    @Override
    protected void onPreExecute() {
        preExecuteRunnable.run();
    }

    @Override
    protected T doInBackground(Void... voids) {
        try {
            return doInBackgroundCallable.call();
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    protected void onPostExecute(T result) {
        postExecuteRunnable.run(result);
    }

    @Override
    protected void onCancelled(T result) {
        cancelledRunnable.run(result);
    }

    void setPreExecuteRunnable(@NonNull Runnable preExecuteRunnable) {
        this.preExecuteRunnable = preExecuteRunnable;
    }

    void setDoInBackgroundCallable(@NonNull Callable<T> doInBackgroundCallable) {
        this.doInBackgroundCallable = doInBackgroundCallable;
    }

    void setPostExecuteRunnable(@NonNull ResultRunnable<T> postExecuteRunnable) {
        this.postExecuteRunnable = postExecuteRunnable;
    }

    void setCancelledRunnable(@NonNull ResultRunnable<T> cancelledRunnable) {
        this.cancelledRunnable = cancelledRunnable;
    }
}
