package drewhamilton.places.async;

import java.util.concurrent.Callable;

class NoOpCallable<V> implements Callable<V> {

    @Override
    public V call() throws Exception {
        return null;
    }
}
