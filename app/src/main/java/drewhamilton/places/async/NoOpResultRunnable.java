package drewhamilton.places.async;

class NoOpResultRunnable<T> implements ResultRunnable<T> {

    @Override
    public void run(T result) {
        // No-op
    }
}
