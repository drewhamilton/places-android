package drewhamilton.places.async;

class NoOpRunnable implements Runnable {

    @Override
    public void run() {
        // No-op
    }
}
