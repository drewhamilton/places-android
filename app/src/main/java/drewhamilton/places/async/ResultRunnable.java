package drewhamilton.places.async;

public interface ResultRunnable<T> {

    void run(T result);
}
