package drewhamilton.places.data;

import java.util.ArrayList;
import java.util.List;

import drewhamilton.places.data.json.JsonPlace;

public class ModelMapper {

    public List<Place> convertToDataModels(List<JsonPlace> jsonModels) {
        List<Place> places = new ArrayList<>(jsonModels.size());
        for (JsonPlace jsonPlace : jsonModels) {
            places.add(convertToDataModel(jsonPlace));
        }

        return places;
    }

    private static Place convertToDataModel(JsonPlace jsonModel) {
        return new Place(jsonModel.id, jsonModel.name, jsonModel.country, jsonModel.coord.lat,
                jsonModel.coord.lon);
    }
}
