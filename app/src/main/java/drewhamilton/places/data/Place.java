package drewhamilton.places.data;

import android.support.annotation.VisibleForTesting;

public class Place {

    public final int id;

    public final String name;
    public final String country;

    public final double latitude;
    public final double longitude;

    @VisibleForTesting(otherwise = VisibleForTesting.PACKAGE_PRIVATE)
    public Place(int id, String name, String country, double latitude, double longitude) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
