package drewhamilton.places.data;

import java.util.ArrayList;
import java.util.List;

import drewhamilton.places.data.json.JsonExtractor;
import drewhamilton.places.data.json.JsonPlace;

public class PlaceRepository {

    private static final int EXPECTED_LIST_SIZE = 209_557;

    private final JsonExtractor jsonExtractor;
    private final ModelMapper modelMapper;

    private final List<Place> places = new ArrayList<>(EXPECTED_LIST_SIZE);

    public PlaceRepository(JsonExtractor jsonExtractor, ModelMapper modelMapper) {
        this.jsonExtractor = jsonExtractor;
        this.modelMapper = modelMapper;
    }

    public List<Place> getPlaces() {
        if (places.isEmpty()) {
            places.addAll(retrievePlacesFromJson());
        }
        return places;
    }

    private List<Place> retrievePlacesFromJson() {
        try {
            final List<JsonPlace> jsonPlaces = jsonExtractor.extractPlaces();
            return modelMapper.convertToDataModels(jsonPlaces);
        } catch (OutOfMemoryError error) {
            throw new RetrievalError(error);
        }
    }

    public static class RetrievalError extends RuntimeException {

        RetrievalError(Throwable cause) {
            super(cause);
        }
    }
}
