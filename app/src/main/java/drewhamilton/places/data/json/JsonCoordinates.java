package drewhamilton.places.data.json;

import android.support.annotation.VisibleForTesting;

public class JsonCoordinates {

    public final double lon;
    public final double lat;

    @VisibleForTesting(otherwise = VisibleForTesting.PACKAGE_PRIVATE)
    public JsonCoordinates(double lon, double lat) {
        this.lon = lon;
        this.lat = lat;
    }
}
