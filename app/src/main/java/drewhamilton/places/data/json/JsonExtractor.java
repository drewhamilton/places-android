package drewhamilton.places.data.json;

import com.google.gson.Gson;

import java.io.Reader;
import java.util.Arrays;
import java.util.List;

import drewhamilton.places.R;
import drewhamilton.places.resources.RawFileExtractor;

public class JsonExtractor {

    private final Gson gson;
    private final RawFileExtractor rawFileExtractor;

    public JsonExtractor(Gson gson, RawFileExtractor rawFileExtractor) {
        this.gson = gson;
        this.rawFileExtractor = rawFileExtractor;
    }

    public List<JsonPlace> extractPlaces() throws OutOfMemoryError {
        final Reader stream = rawFileExtractor.extract(R.raw.places);
        JsonPlace[] result = gson.fromJson(stream, JsonPlace[].class);
        return Arrays.asList(result);
    }
}
