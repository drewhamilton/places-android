package drewhamilton.places.data.json;

import android.support.annotation.VisibleForTesting;

import com.google.gson.annotations.SerializedName;

public class JsonPlace {

    public final String country;
    public final String name;
    @SerializedName("_id") public final int id;
    public final JsonCoordinates coord;

    @VisibleForTesting(otherwise = VisibleForTesting.PACKAGE_PRIVATE)
    public JsonPlace(String country, String name, int id, JsonCoordinates coord) {
        this.country = country;
        this.name = name;
        this.id = id;
        this.coord = coord;
    }
}
