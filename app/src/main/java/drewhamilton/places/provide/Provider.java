package drewhamilton.places.provide;

import android.content.Context;
import android.support.annotation.VisibleForTesting;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import drewhamilton.places.async.AsyncChainProvider;
import drewhamilton.places.data.ModelMapper;
import drewhamilton.places.data.PlaceRepository;
import drewhamilton.places.data.json.JsonExtractor;
import drewhamilton.places.resources.RawFileExtractor;
import drewhamilton.places.ui.PresenterFactory;

/**
 * Provides all important class instances throughout the app.
 *
 * Manages singletons for classes that are singletons.
 *
 * Essentially handles what Dagger would normally handle.
 */
public class Provider {

    // Normally Contexts in static fields are bad, but this is the application Context so it's fine
    private static Provider instance;

    //region Singletons
    private static PresenterFactory presenterFactory;
    private static PlaceRepository placeRepository;
    //endregion

    private Context applicationContext;

    public static void initialize(Context context) {
        instance = new Provider(context);
    }

    public static Provider instance() {
        return instance;
    }

    @VisibleForTesting
    public static void overrideInstance(Provider instance) {
        Provider.instance = instance;
    }

    @VisibleForTesting
    Provider(Context context) {
        applicationContext = context.getApplicationContext();
    }

    public PresenterFactory presenterFactory() {
        if (presenterFactory == null) {
            presenterFactory = newPresenterFactory();
        }

        return presenterFactory;
    }

    public PlaceRepository placeRepository() {
        if (placeRepository == null) {
            placeRepository = newPlaceRepository();
        }
        return placeRepository;
    }

    public AsyncChainProvider asyncChainProvider() {
        return newAsyncChainProvider();
    }

    @VisibleForTesting
    PresenterFactory newPresenterFactory() {
        return new PresenterFactory(instance);
    }

    @VisibleForTesting
    PlaceRepository newPlaceRepository() {
        return new PlaceRepository(jsonExtractor(), modelMapper());
    }

    @VisibleForTesting
    AsyncChainProvider newAsyncChainProvider() {
        return new AsyncChainProvider();
    }

    private ModelMapper modelMapper() {
        return new ModelMapper();
    }

    private JsonExtractor jsonExtractor() {
        return new JsonExtractor(gson(), rawFileExtractor());
    }

    private RawFileExtractor rawFileExtractor() {
        return new RawFileExtractor(applicationContext.getResources());
    }

    private Gson gson() {
        return new GsonBuilder().create();
    }
}
