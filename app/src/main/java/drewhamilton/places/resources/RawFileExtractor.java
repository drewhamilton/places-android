package drewhamilton.places.resources;

import android.content.res.Resources;
import android.support.annotation.RawRes;

import java.io.InputStreamReader;
import java.io.Reader;

public class RawFileExtractor {

    private final Resources resources;

    public RawFileExtractor(Resources resources) {
        this.resources = resources;
    }

    public Reader extract(@RawRes int rawResource) {
        return new InputStreamReader(resources.openRawResource(rawResource));
    }
}
