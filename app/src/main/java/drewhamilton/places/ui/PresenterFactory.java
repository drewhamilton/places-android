package drewhamilton.places.ui;

import drewhamilton.places.provide.Provider;
import drewhamilton.places.ui.list.ListPresenter;

public class PresenterFactory {

    private final Provider provider;

    public PresenterFactory(Provider provider) {
        this.provider = provider;
    }

    public ListPresenter listPresenter() {
        return new ListPresenter(provider.asyncChainProvider(), provider.placeRepository());
    }
}
