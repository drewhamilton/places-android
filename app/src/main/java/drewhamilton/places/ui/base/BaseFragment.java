package drewhamilton.places.ui.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseFragment<V, P extends Presenter<V>> extends Fragment {

    PresenterRetainer<P> presenterRetainer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayout(), container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        attachToPresenter();
    }

    @Override
    public void onDestroy() {
        getPresenter().detachView();
        super.onDestroy();
    }

    @LayoutRes
    protected abstract int getLayout();

    protected abstract P newPresenter();

    protected P getPresenter() {
        return presenterRetainer.getPresenter();
    }

    private void attachToPresenter() {
        final V view;
        try {
            view = thisAsView();
        } catch (ClassCastException ex) {
            throw new InvalidViewImplementationException();
        }

        presenterRetainer = PresenterRetainer.initialize(getChildFragmentManager(), getClass().getName());
        P presenter = presenterRetainer.getPresenter();
        if (presenter == null) {
            presenter = newPresenter();
            presenterRetainer.setPresenter(presenter);
            presenter.attachView(view);
        } else {
            presenter.reattachView(view);
        }
    }

    private V thisAsView() {
        return (V) this;
    }

    private static class InvalidViewImplementationException extends RuntimeException {

        InvalidViewImplementationException() {
            super("All BaseActivity subclasses must implement the MvpView type by which they are parameterized");
        }
    }
}
