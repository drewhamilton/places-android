package drewhamilton.places.ui.base;

public abstract class Presenter<V> {

    private V view;

    /**
     * Attach a new View
     *
     * @throws ViewAlreadyAttachedException if this Presenter already has a View attached
     */
    protected void attachView(V view) {
        if (isViewAttached()) {
            throw new ViewAlreadyAttachedException();
        } else {
            this.view = view;
        }
    }

    /**
     * Reattach a View to a this Presenter; e.g. when the configuration changed and the system has
     * spun up a new Activity or Fragment.
     *
     * Does *not* call detachView().
     */
    protected void reattachView(V view) {
        this.view = view;
    }

    protected void detachView() {
        this.view = null;
    }

    protected boolean isViewAttached() {
        return view != null;
    }

    protected V getView() {
        return view;
    }

    public static class ViewAlreadyAttachedException extends RuntimeException {

        ViewAlreadyAttachedException() {
            super("Tried to attach a view when another view was already attached");
        }
    }
}
