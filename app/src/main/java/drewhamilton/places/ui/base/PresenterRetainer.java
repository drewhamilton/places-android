package drewhamilton.places.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

class PresenterRetainer<P extends Presenter> {

    private DataFragment<P> dataFragment;

    public static <P extends Presenter> PresenterRetainer<P> initialize(FragmentManager fragmentManager, String tag) {
        PresenterRetainer<P> presenterRetainer = new PresenterRetainer<>();
        presenterRetainer.dataFragment = (DataFragment<P>) fragmentManager.findFragmentByTag(tag);

        if (presenterRetainer.dataFragment == null) {
            presenterRetainer.dataFragment = new DataFragment<>();
            fragmentManager.beginTransaction()
                    .add(presenterRetainer.dataFragment, tag)
                    .commit();
        }

        return presenterRetainer;
    }

    private PresenterRetainer() {}

    public P getPresenter() {
        return dataFragment.data;
    }

    public void setPresenter(P data) {
        dataFragment.data = data;
    }

    @VisibleForTesting
    // Actually visible for Android, but should not be used directly in production
    public static class DataFragment<D> extends Fragment {

        private D data;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }
    }
}
