package drewhamilton.places.ui.list;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import drewhamilton.places.R;
import drewhamilton.places.provide.Provider;
import drewhamilton.places.ui.base.BaseFragment;
import drewhamilton.places.ui.map.MappedPlaceModel;

public class ListFragment extends BaseFragment<ListView, ListPresenter>
        implements ListView, PlaceListAdapter.ItemClickListener {

    private static final String MAP_SCHEME = "geo";
    private static final String COORDINATE_DATA_TEMPLATE = "%f,%f";

    private final PlaceListAdapter adapter = new PlaceListAdapter(this);

    private View waitingView;
    private View filterContainer;
    private RecyclerView listView;

    @Override
    protected int getLayout() {
        return R.layout.fragment_list;
    }

    @Override
    protected ListPresenter newPresenter() {
        return Provider.instance().presenterFactory().listPresenter();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        waitingView = view.findViewById(R.id.waiting);
        filterContainer = view.findViewById(R.id.filter_container);
        listView = view.findViewById(R.id.list);

        super.onViewCreated(view, savedInstanceState);

        final TextView filterView = filterContainer.findViewById(R.id.filter);
        filterView.addTextChangedListener(new FilterTextWatcher());

        listView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        listView.setAdapter(adapter);
    }

    //region ListView
    @Override
    public void displayPlacesInList(List<ListedPlaceModel> places) {
        adapter.displayItems(places);

        waitingView.setVisibility(View.GONE);
        filterContainer.setVisibility(View.VISIBLE);
        listView.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayPlaceOnMap(MappedPlaceModel place) {
        final String uriSsp = String.format(Locale.getDefault(), COORDINATE_DATA_TEMPLATE, place.latitude, place.longitude);
        final Uri coordinateUri = Uri.fromParts(MAP_SCHEME, uriSsp, null);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, coordinateUri);
        startActivity(mapIntent);
    }
    //endregion

    //region ItemClickListener
    @Override
    public void onItemClick(ListedPlaceModel listedPlaceModel) {
        getPresenter().showMap(listedPlaceModel);
    }
    //endregion

    private class FilterTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void afterTextChanged(Editable s) {
            getPresenter().applyFilter(s.toString());
        }
    }
}
