package drewhamilton.places.ui.list;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import drewhamilton.places.async.AsyncChainProvider;
import drewhamilton.places.async.AsyncSubscription;
import drewhamilton.places.data.Place;
import drewhamilton.places.data.PlaceRepository;
import drewhamilton.places.ui.base.Presenter;
import drewhamilton.places.ui.map.MappedPlaceModel;

public class ListPresenter extends Presenter<ListView> {

    private static final String FULL_PLACE_NAME_TEMPLATE = "%s, %s";

    private final AsyncChainProvider asyncChainProvider;
    private final PlaceRepository repository;

    private AsyncSubscription placeLoadingSubscription;
    private AsyncSubscription modelConversionSubscription;
    private AsyncSubscription filterSubscription;

    @NonNull private final Map<Integer, Place> placeIdMap = new HashMap<>();

    /*
     * The filtering will be done on this list.
     *
     * Efficiency notes:
     *
     * - This is far more efficient than parsing the whole list from JSON for every search, because
     *   reading files and parsing text are both expensive operations.
     *
     * - This is noticeably more efficient than storing the Places list and re-converting it to a
     *   UI model list for every search, probably because String formatting and object creation are
     *   fairly expensive.
     *
     * - This is less efficient than using a Prefix Tree to store the place name
     *   Strings, but in practice using the List is quite fast no modern devices. Implementing an
     *   entire Prefix Tree seemed like overkill on such a short time frame. If supporting users
     *   with old devices were a priority I would update this to use a Prefix Tree (hopefully from
     *   a third-party library).
     */
    @NonNull private final List<ListedPlaceModel> allUiModels = new ArrayList<>();

    @NonNull private List<ListedPlaceModel> filteredUiModels = allUiModels;

    public ListPresenter(AsyncChainProvider asyncChainProvider, PlaceRepository repository) {
        this.asyncChainProvider = asyncChainProvider;
        this.repository = repository;
    }

    @Override
    protected void attachView(ListView view) {
        super.attachView(view);
        loadPlaces();
    }

    @Override
    protected void reattachView(ListView view) {
        super.reattachView(view);
        if (allUiModels.isEmpty()) {
            loadPlaces();
        } else {
            displayPlaces(filteredUiModels);
        }
    }

    @Override
    protected void detachView() {
        cancelSubscriptions();
        super.detachView();
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PACKAGE_PRIVATE)
    protected void showMap(ListedPlaceModel listedPlaceModel) {
        if (isViewAttached()) {
            final Place place = placeIdMap.get(listedPlaceModel.id);
            final MappedPlaceModel mappedPlace = new MappedPlaceModel(place.latitude, place.longitude);
            getView().displayPlaceOnMap(mappedPlace);
        }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PACKAGE_PRIVATE)
    protected void applyFilter(String filter) {
        cancelFilterSubscription();
        filterSubscription = asyncChainProvider.single(() -> filterUiModels(filter))
                .postExecuteOnUiThread(filteredUiModels -> {
                    this.filteredUiModels = filteredUiModels;

                    // Only display filtered places if we actually have any places:
                    if (!allUiModels.isEmpty()) {
                        displayPlaces(filteredUiModels);
                    }
                })
                .subscribe();
    }

    private void loadPlaces() {
        placeLoadingSubscription = asyncChainProvider.single(repository::getPlaces)
                .postExecuteOnUiThread(this::updatePlaces)
                .subscribe();
    }

    private void updatePlaces(List<Place> places) {
        cacheById(places);
        displayPlaces(places);
    }

    private void displayPlaces(final Collection<Place> places) {
        modelConversionSubscription = asyncChainProvider.single(() -> convertToUiModels(places))
                .postExecuteOnUiThread(uiModels -> {
                    Collections.sort(uiModels, new AlphabeticalComparator());
                    allUiModels.clear();
                    allUiModels.addAll(uiModels);
                    displayPlaces(allUiModels);
                })
                .subscribe();
    }

    private void displayPlaces(final List<ListedPlaceModel> places) {
        if (isViewAttached()) {
            getView().displayPlacesInList(places);
        }
    }

    private List<ListedPlaceModel> filterUiModels(String filter) {
        final List<ListedPlaceModel> filteredUiModels;
        if (filter.isEmpty()) {
            filteredUiModels = allUiModels;
        } else {
            filteredUiModels = new ArrayList<>();
            for (ListedPlaceModel uiModel : allUiModels) {
                if (matchesFilter(uiModel, filter)) {
                    filteredUiModels.add(uiModel);
                }
            }
        }

        return filteredUiModels;
    }

    private void cacheById(List<Place> places) {
        for (Place place : places) {
            placeIdMap.put(place.id, place);
        }
    }

    private void cancelSubscriptions() {
        if (placeLoadingSubscription != null) {
            placeLoadingSubscription.cancel();
            placeLoadingSubscription = null;
        }

        if (modelConversionSubscription != null) {
            modelConversionSubscription.cancel();
            modelConversionSubscription = null;
        }

        cancelFilterSubscription();
    }

    private void cancelFilterSubscription() {
        if (filterSubscription != null) {
            filterSubscription.cancel();
            filterSubscription = null;
        }
    }

    private static boolean matchesFilter(ListedPlaceModel uiModel, String filter) {
        return uiModel.fullName.toLowerCase().startsWith(filter.toLowerCase());
    }

    private static List<ListedPlaceModel> convertToUiModels(Collection<Place> dataModels) {
        List<ListedPlaceModel> uiModels = new ArrayList<>(dataModels.size());
        for (Place dataModel : dataModels) {
            uiModels.add(convertToUiModel(dataModel));
        }
        return uiModels;
    }

    private static ListedPlaceModel convertToUiModel(Place dataModel) {
        final String fullPlaceName = String.format(Locale.getDefault(), FULL_PLACE_NAME_TEMPLATE,
                dataModel.name, dataModel.country);
        return new ListedPlaceModel(dataModel.id, fullPlaceName);
    }

    private static class AlphabeticalComparator implements Comparator<ListedPlaceModel> {

        @Override
        public int compare(ListedPlaceModel modelA, ListedPlaceModel modelB) {
            return modelA.fullName.compareTo(modelB.fullName);
        }
    }
}
