package drewhamilton.places.ui.list;

import java.util.List;

import drewhamilton.places.ui.map.MappedPlaceModel;

public interface ListView {

    void displayPlacesInList(List<ListedPlaceModel> places);

    void displayPlaceOnMap(MappedPlaceModel place);
}
