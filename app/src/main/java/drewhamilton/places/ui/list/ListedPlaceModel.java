package drewhamilton.places.ui.list;

public class ListedPlaceModel {

    public final int id;
    public final String fullName;

    ListedPlaceModel(int id, String fullName) {
        this.id = id;
        this.fullName = fullName;
    }
}
