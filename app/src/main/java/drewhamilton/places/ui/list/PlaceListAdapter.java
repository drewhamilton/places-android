package drewhamilton.places.ui.list;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import drewhamilton.places.R;

public class PlaceListAdapter extends RecyclerView.Adapter<PlaceListAdapter.ListedPlaceViewHolder> {

    @LayoutRes private static final int LIST_ITEM_LAYOUT = R.layout.view_list_item;

    private static final int DIFF_UTIL_MAX_ITEM_COUNT = 1000;

    @NonNull private final List<ListedPlaceModel> items = new ArrayList<>();

    @NonNull private final ItemClickListener clickListener;

    PlaceListAdapter(@NonNull ItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public ListedPlaceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(LIST_ITEM_LAYOUT, parent, false);
        return new ListedPlaceViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ListedPlaceViewHolder holder, int position) {
        final ListedPlaceModel item = items.get(position);
        holder.bind(item, clickListener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    void displayItems(@NonNull List<ListedPlaceModel> items) {
        List<ListedPlaceModel> oldItems = new ArrayList<>(this.items);
        this.items.clear();
        this.items.addAll(items);

        // The DiffUtil freezes up the UI thread if there are too many items to diff:
        if (this.items.size() > DIFF_UTIL_MAX_ITEM_COUNT || oldItems.size() > DIFF_UTIL_MAX_ITEM_COUNT) {
            notifyDataSetChanged();
        } else {
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtilCallback(oldItems, this.items));
            diffResult.dispatchUpdatesTo(this);
        }

    }

    static class ListedPlaceViewHolder extends RecyclerView.ViewHolder {

        private final TextView nameView;

        ListedPlaceViewHolder(View itemView) {
            super(itemView);
            nameView = itemView.findViewById(R.id.name);
        }

        void bind(final ListedPlaceModel item, ItemClickListener itemClickListener) {
            nameView.setText(item.fullName);
            itemView.setOnClickListener(v -> itemClickListener.onItemClick(item));
        }
    }

    private static class DiffUtilCallback extends DiffUtil.Callback {

        private final List<ListedPlaceModel> oldItems;
        private final List<ListedPlaceModel> newItems;

        DiffUtilCallback(List<ListedPlaceModel> oldItems, List<ListedPlaceModel> newItems) {
            this.oldItems = oldItems;
            this.newItems = newItems;
        }

        @Override
        public int getOldListSize() {
            return oldItems.size();
        }

        @Override
        public int getNewListSize() {
            return newItems.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldItems.get(oldItemPosition).id == newItems.get(newItemPosition).id;
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return areItemsTheSame(oldItemPosition, newItemPosition);
        }
    }

    interface ItemClickListener {
        void onItemClick(ListedPlaceModel listedPlaceModel);
    }
}
