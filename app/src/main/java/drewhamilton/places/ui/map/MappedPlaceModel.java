package drewhamilton.places.ui.map;

public class MappedPlaceModel {

    public final double latitude;
    public final double longitude;

    public MappedPlaceModel(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
