package drewhamilton.places.async;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.Callable;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class AsyncChainTest {

    @Mock private ChainableAsyncTask<String> mockAsyncTask;

    @InjectMocks private AsyncChain<String> asyncChain;

    @Test
    public void chainedMethods_callExpectedSetters() {
        final Runnable mockPreExecute = mock(Runnable.class);
        final Callable<String> mockBackgroundExecute = mock(Callable.class);
        final ResultRunnable<String> mockPostExecute = mock(ResultRunnable.class);
        final ResultRunnable<String> mockOnCancelled = mock(ResultRunnable.class);

        asyncChain
                .preExecuteOnUiThread(mockPreExecute)
                .executeOnBackgroundThread(mockBackgroundExecute)
                .postExecuteOnUiThread(mockPostExecute)
                .executeOnCancelledOnUiThread(mockOnCancelled);

        verify(mockAsyncTask).setPreExecuteRunnable(mockPreExecute);
        verify(mockAsyncTask).setDoInBackgroundCallable(mockBackgroundExecute);
        verify(mockAsyncTask).setPostExecuteRunnable(mockPostExecute);
        verify(mockAsyncTask).setCancelledRunnable(mockOnCancelled);
        verifyNoMoreInteractions(mockAsyncTask);
    }

    @Test
    public void subscribe_executesAsyncTask() {
        AsyncSubscription result = asyncChain.subscribe();
        verify(mockAsyncTask).execute();
        verifyNoMoreInteractions(mockAsyncTask);

        assertNotNull(result);
    }
}
