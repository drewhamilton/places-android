package drewhamilton.places.async;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AsyncSubscriptionTest {

    @Mock private ChainableAsyncTask mockAsyncTask;

    @InjectMocks private AsyncSubscription asyncSubscription;

    @Test
    public void cancel_cancelsAsyncTask() {
        asyncSubscription.cancel();

        verify(mockAsyncTask).cancel(true);
    }
}
