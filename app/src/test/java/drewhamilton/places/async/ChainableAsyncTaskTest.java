package drewhamilton.places.async;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.Callable;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ChainableAsyncTaskTest {

    private ChainableAsyncTask<String> chainableAsyncTask;

    @Before
    public void setUp() {
        chainableAsyncTask = new ChainableAsyncTask<>();
    }

    @Test
    public void preExecute_runsPreExecuteRunnable() {
        final Runnable mockPreExecuteRunnable = mock(Runnable.class);

        chainableAsyncTask.setPreExecuteRunnable(mockPreExecuteRunnable);
        chainableAsyncTask.onPreExecute();

        verify(mockPreExecuteRunnable).run();
    }

    @Test
    public void doInBackground_callsDoInBackgroundCallable() throws Exception {
        final Callable<String> mockDoInBackgroundCallable = mock(Callable.class);

        chainableAsyncTask.setDoInBackgroundCallable(mockDoInBackgroundCallable);
        chainableAsyncTask.doInBackground((Void) null);

        verify(mockDoInBackgroundCallable).call();
    }

    @Test
    public void postExecute_callsPostExecuteRunnable() {
        final ResultRunnable<String> mockPostExecuteRunnable = mock(ResultRunnable.class);
        final String testResult = "Test result";

        chainableAsyncTask.setPostExecuteRunnable(mockPostExecuteRunnable);
        chainableAsyncTask.onPostExecute(testResult);

        verify(mockPostExecuteRunnable).run(testResult);
    }

    @Test
    public void cancelled_callsCancelledRunnable() {
        final ResultRunnable<String> mockCancelledRunnable = mock(ResultRunnable.class);
        final String testResult = "Test result";

        chainableAsyncTask.setCancelledRunnable(mockCancelledRunnable);
        chainableAsyncTask.onCancelled(testResult);

        verify(mockCancelledRunnable).run(testResult);
    }

    @Test
    public void noRunnablesSet_doesNotCrash() {
        chainableAsyncTask.onPreExecute();
        chainableAsyncTask.doInBackground((Void) null);
        chainableAsyncTask.onPostExecute(null);
        chainableAsyncTask.onCancelled(null);
    }
}
