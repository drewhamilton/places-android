package drewhamilton.places.async;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;

/**
 * Runs the AsyncTask synchronously when subscribe() is called
 */
public class FakeAsyncChain<T> extends AsyncChain<T> {

    private final List<AsyncSubscription> mockSubscriptions = new ArrayList<>();

    private ExecutionMode executionMode = ExecutionMode.NORMAL;

    FakeAsyncChain(ChainableAsyncTask<T> asyncTask) {
        super(asyncTask);
    }

    @Override
    public AsyncSubscription subscribe() {
        getAsyncTask().onPreExecute();
        final T result = getAsyncTask().doInBackground((Void) null);

        switch (executionMode) {
            case CANCELLED:
                getAsyncTask().onCancelled(result);
                break;
            case NORMAL:
            default:
                getAsyncTask().onPostExecute(result);
                break;
        }

        AsyncSubscription mockSubscription = mock(AsyncSubscription.class);
        mockSubscriptions.add(mockSubscription);
        return mockSubscription;
    }

    public List<AsyncSubscription> getMockSubscriptions() {
        return mockSubscriptions;
    }

    public void setExecutionMode(ExecutionMode executionMode) {
        this.executionMode = executionMode;
    }

    public enum ExecutionMode {
        NORMAL,
        CANCELLED
    }
}
