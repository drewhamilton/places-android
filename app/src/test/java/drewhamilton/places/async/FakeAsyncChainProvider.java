package drewhamilton.places.async;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class FakeAsyncChainProvider extends AsyncChainProvider {

    private final List<FakeAsyncChain> providedAsyncChains = new ArrayList<>();

    @Override
    public <T> AsyncChain<T> single(Callable<T> executeOnBackgroundThread) {
        AsyncChain<T> fakeAsyncChain = new FakeAsyncChain<T>(new ChainableAsyncTask<>())
                .executeOnBackgroundThread(executeOnBackgroundThread);
        providedAsyncChains.add((FakeAsyncChain) fakeAsyncChain);
        return fakeAsyncChain;
    }

    public List<FakeAsyncChain> getProvidedAsyncChains() {
        return providedAsyncChains;
    }
}
