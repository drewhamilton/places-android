package drewhamilton.places.data;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import drewhamilton.places.data.json.JsonCoordinates;
import drewhamilton.places.data.json.JsonPlace;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ModelMapperTest {

    private static final String TEST_NAME = "Name %d";
    private static final String TEST_COUNTRY = "Country %d";

    private ModelMapper modelMapper;

    @Before
    public void setUp() {
        modelMapper = new ModelMapper();
    }

    @Test
    public void convertToDataModels_convertsExpectedFields() {
        final int count = 823;
        final List<Place> expectedOutput = createExpectedOutputModels(count);

        final List<Place> actualOutput = modelMapper.convertToDataModels(createInputModels(count));

        assertNotNull(actualOutput);
        assertEquals(expectedOutput.size(), actualOutput.size());
        for (int i = 0; i < expectedOutput.size(); ++i) {
            final Place expectedPlace = expectedOutput.get(i);
            final Place actualPlace = actualOutput.get(i);

            assertEquals(expectedPlace.id, actualPlace.id);
            assertEquals(expectedPlace.name, actualPlace.name);
            assertEquals(expectedPlace.country, actualPlace.country);
            assertEquals(expectedPlace.latitude, actualPlace.latitude, 0.000_000_1);
            assertEquals(expectedPlace.longitude, actualPlace.longitude, 0.000_000_1);
        }
    }

    private static List<JsonPlace> createInputModels(int count) {
        final List<JsonPlace> inputModels = new ArrayList<>(count);
        for (int i = 0; i < count; ++i) {
            inputModels.add(new JsonPlace(createExpectedCountry(i), createExpectedName(i), i,
                    createExpectedCoordinates(i)));
        }
        return inputModels;
    }

    private static List<Place> createExpectedOutputModels(int count) {
        final List<Place> outputModels = new ArrayList<>(count);
        for (int i = 0; i < count; ++i) {
            JsonCoordinates expectedCoordinates = createExpectedCoordinates(i);
            outputModels.add(new Place(i, createExpectedName(i), createExpectedCountry(i),
                    expectedCoordinates.lat, expectedCoordinates.lon));
        }
        return outputModels;
    }

    private static String createExpectedName(int position) {
        return String.format(Locale.US, TEST_NAME, position);
    }

    private static String createExpectedCountry(int position) {
        return String.format(Locale.US, TEST_COUNTRY, position);
    }

    private static JsonCoordinates createExpectedCoordinates(int position) {
        return new JsonCoordinates((double) position, (double) position);
    }
}
