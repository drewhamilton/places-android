package drewhamilton.places.data;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import drewhamilton.places.data.json.JsonExtractor;
import drewhamilton.places.data.json.JsonPlace;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlaceRepositoryTest {

    @Mock private JsonExtractor mockJsonExtractor;
    @Mock private ModelMapper mockModelMapper;

    @InjectMocks private PlaceRepository placeRepository;

    @Test
    public void getPlaces_firstTime_retrievesFromJsonExtractor() {
        final List<JsonPlace> intermediateList = Collections.singletonList(new JsonPlace(null, null, 1, null));
        when(mockJsonExtractor.extractPlaces()).thenReturn(intermediateList);

        final List<Place> testList = Collections.singletonList(new Place(1, null, null, 0d, 0d));
        when(mockModelMapper.convertToDataModels(intermediateList)).thenReturn(testList);

        final List<Place> result = placeRepository.getPlaces();

        verify(mockJsonExtractor).extractPlaces();
        verifyNoMoreInteractions(mockJsonExtractor);

        assertEquals(testList, result);
    }

    @Test
    public void getPlaces_subsequentTimes_returnsFromMemory() {
        final List<JsonPlace> intermediateList = Collections.singletonList(new JsonPlace(null, null, 1, null));
        when(mockJsonExtractor.extractPlaces()).thenReturn(intermediateList);

        final List<Place> testList = Collections.singletonList(new Place(1, null, null, 0d, 0d));
        when(mockModelMapper.convertToDataModels(intermediateList)).thenReturn(testList);

        placeRepository.getPlaces();

        verify(mockJsonExtractor).extractPlaces();
        verify(mockModelMapper).convertToDataModels(intermediateList);
        verifyNoMoreInteractions(mockJsonExtractor);
        verifyNoMoreInteractions(mockModelMapper);

        for (int i = 0; i < 100; ++i) {
            final List<Place> result = placeRepository.getPlaces();
            verifyNoMoreInteractions(mockJsonExtractor);
            verifyNoMoreInteractions(mockModelMapper);
            assertEquals(testList, result);
        }
    }

    @Test(expected = PlaceRepository.RetrievalError.class)
    public void getPlaces_outOfMemoryError_convertedToRetrievalError() {
        when(mockJsonExtractor.extractPlaces()).thenThrow(new OutOfMemoryError("Test error"));
        placeRepository.getPlaces();
    }
}
