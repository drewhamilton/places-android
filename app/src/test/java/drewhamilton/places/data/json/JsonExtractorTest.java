package drewhamilton.places.data.json;

import com.google.gson.Gson;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.Reader;
import java.util.List;

import drewhamilton.places.R;
import drewhamilton.places.resources.RawFileExtractor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JsonExtractorTest {

    @Mock private Gson mockGson;
    @Mock private RawFileExtractor mockRawFileExtractor;

    @InjectMocks private JsonExtractor jsonExtractor;

    @Test
    public void extractJson() {
        final Reader mockReader = mock(Reader.class);
        when(mockRawFileExtractor.extract(R.raw.places)).thenReturn(mockReader);

        final JsonPlace[] testJsonPlaces = createTestJsonPlaces(456);
        when(mockGson.fromJson(mockReader, JsonPlace[].class)).thenReturn(testJsonPlaces);

        List<JsonPlace> result = jsonExtractor.extractPlaces();
        assertNotNull(result);
        assertEquals(testJsonPlaces.length, result.size());
        for (int i = 0; i < testJsonPlaces.length; ++i) {
            assertEquals(testJsonPlaces[i].id, result.get(i).id);
        }
    }

    private static JsonPlace[] createTestJsonPlaces(int count) {
        final JsonPlace[] testJsonPlaces = new JsonPlace[count];
        for (int i = 0; i < count; ++i) {
            final JsonPlace testJsonPlace = new JsonPlace(null, null, i, null);
            testJsonPlaces[i] = testJsonPlace;
        }

        return testJsonPlaces;
    }
}
