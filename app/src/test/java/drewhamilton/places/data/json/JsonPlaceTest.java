package drewhamilton.places.data.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class JsonPlaceTest {

    private static final String TEST_JSON = "[{\"country\":\"UA\",\"name\":\"Hurzuf\"," +
            "\"_id\":707860,\"coord\":{\"lon\":34.283333,\"lat\":44.549999}}]";

    private Gson gson;

    @Before
    public void setUp() {
        gson = new GsonBuilder().create();
    }

    @Test
    public void modelMatchesJson() {
        JsonPlace[] places = gson.fromJson(TEST_JSON, JsonPlace[].class);

        assertNotNull(places);
        assertEquals(1, places.length);

        final JsonPlace model = places[0];
        assertNotNull(model);
        assertEquals("UA", model.country);
        assertEquals("Hurzuf", model.name);
        assertEquals(707860, model.id);
        assertEquals(34.283333, model.coord.lon, 0.000_000_1);
        assertEquals(44.549999, model.coord.lat, 0.000_000_1);
    }
}
