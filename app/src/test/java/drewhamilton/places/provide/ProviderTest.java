package drewhamilton.places.provide;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import drewhamilton.places.async.AsyncChainProvider;
import drewhamilton.places.data.PlaceRepository;
import drewhamilton.places.ui.PresenterFactory;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProviderTest {

    @Mock private Context mockContext;

    @Before
    public void setUp() {
        when(mockContext.getApplicationContext()).thenReturn(mockContext);

        Provider.initialize(mockContext);
    }

    @Test
    public void presenterFactory_isSingleton() {
        final PresenterFactory presenterFactory1 = Provider.instance().presenterFactory();
        final PresenterFactory presenterFactory2 = Provider.instance().presenterFactory();

        assertNotNull(presenterFactory1);
        assertSame(presenterFactory1, presenterFactory2);
    }

    @Test
    public void placeRepository_isSingleton() {
        final PlaceRepository placeRepository1 = Provider.instance().placeRepository();
        final PlaceRepository placeRepository2 = Provider.instance().placeRepository();

        assertNotNull(placeRepository1);
        assertSame(placeRepository1, placeRepository2);
    }

    @Test
    public void asyncChainProvider_isNotSingleton() {
        final AsyncChainProvider asyncChainProvider1 = Provider.instance().asyncChainProvider();
        final AsyncChainProvider asyncChainProvider2 = Provider.instance().asyncChainProvider();

        assertNotNull(asyncChainProvider1);
        assertNotNull(asyncChainProvider2);
        assertNotSame(asyncChainProvider1, asyncChainProvider2);
    }
}
