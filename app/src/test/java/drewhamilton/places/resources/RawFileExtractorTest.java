package drewhamilton.places.resources;

import android.content.res.Resources;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RawFileExtractorTest {

    private static final int TEST_RESOURCE = 234;

    @Mock private Resources mockResources;

    @InjectMocks private RawFileExtractor rawFileExtractor;

    @Test
    public void openRawResource_delegatesToResources() {
        InputStream mockInputStream = mock(InputStream.class);
        when(mockResources.openRawResource(TEST_RESOURCE)).thenReturn(mockInputStream);

        Reader reader = rawFileExtractor.extract(TEST_RESOURCE);
        verify(mockResources).openRawResource(TEST_RESOURCE);
        assertTrue(reader instanceof InputStreamReader);
    }

    // This is the best I can do to verify the InputStreamReader constructor is called
    @Test(expected = NullPointerException.class)
    public void openRawResource_withoutMockedInputStream_throwsNpe() {
        rawFileExtractor.extract(TEST_RESOURCE);
    }
}
