package drewhamilton.places.ui;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import drewhamilton.places.async.AsyncChainProvider;
import drewhamilton.places.data.PlaceRepository;
import drewhamilton.places.provide.Provider;
import drewhamilton.places.ui.list.ListPresenter;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PresenterFactoryTest {

    @Mock private Provider mockProvider;

    @InjectMocks private PresenterFactory presenterFactory;

    @Test
    public void listPresenter_isNotSingleton() {
        final AsyncChainProvider mockAsyncChainProvider = mock(AsyncChainProvider.class);
        final PlaceRepository mockPlaceRepository = mock(PlaceRepository.class);
        when(mockProvider.asyncChainProvider()).thenReturn(mockAsyncChainProvider);
        when(mockProvider.placeRepository()).thenReturn(mockPlaceRepository);

        final ListPresenter listPresenter1 = presenterFactory.listPresenter();
        verify(mockProvider).asyncChainProvider();
        verify(mockProvider).placeRepository();
        verifyNoMoreInteractions(mockProvider);

        final ListPresenter listPresenter2 = presenterFactory.listPresenter();
        verify(mockProvider, times(2)).asyncChainProvider();
        verify(mockProvider, times(2)).placeRepository();
        verifyNoMoreInteractions(mockProvider);

        assertNotNull(listPresenter1);
        assertNotNull(listPresenter2);
        assertNotSame(listPresenter1, listPresenter2);
    }
}
