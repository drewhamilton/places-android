package drewhamilton.places.ui.base;

import android.support.annotation.CallSuper;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public abstract class PresenterTest<V, P extends Presenter<V>> {

    private V mockView;

    @Before
    @CallSuper
    public void setUp() {
        mockView = mock(getViewClass());
    }

    @Test
    public void attachView_retainsView() {
        getPresenter().attachView(getMockView());

        assertTrue(getPresenter().isViewAttached());
        assertEquals(getMockView(), getPresenter().getView());
    }

    @Test(expected = Presenter.ViewAlreadyAttachedException.class)
    public void attachView_viewAlreadyAttached_throwsException() {
        getPresenter().attachView(getMockView());
        getPresenter().attachView(mock(getViewClass()));
    }

    @Test
    public void detachView_forgetsView() {
        getPresenter().attachView(getMockView());
        getPresenter().detachView();

        assertFalse(getPresenter().isViewAttached());
        assertNull(getPresenter().getView());
    }

    protected abstract Class<V> getViewClass();

    protected abstract P getPresenter();

    protected V getMockView() {
        return mockView;
    }
}
