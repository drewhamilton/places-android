package drewhamilton.places.ui.list;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import drewhamilton.places.async.AsyncSubscription;
import drewhamilton.places.async.FakeAsyncChain;
import drewhamilton.places.async.FakeAsyncChainProvider;
import drewhamilton.places.data.Place;
import drewhamilton.places.data.PlaceRepository;
import drewhamilton.places.ui.base.PresenterTest;
import drewhamilton.places.ui.map.MappedPlaceModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ListPresenterTest extends PresenterTest<ListView, ListPresenter> {

    private static final String TEST_NAME = "Name %s";
    private static final String TEST_COUNTRY = "Country %d";

    private FakeAsyncChainProvider fakeAsyncChainProvider;
    @Mock private PlaceRepository mockPlaceRepository;

    private ListPresenter presenter;

    private List<Place> testPlaces;
    private List<ListedPlaceModel> expectedUiModels;

    @Captor private ArgumentCaptor<List<ListedPlaceModel>> uiModelListCaptor;
    @Captor private ArgumentCaptor<MappedPlaceModel> mappedPlaceModelCaptor;

    @Override
    protected Class<ListView> getViewClass() {
        return ListView.class;
    }

    @Override
    protected ListPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setUp() {
        super.setUp();

        testPlaces = createPlaceList(237);
        expectedUiModels = createExpectedUiModelList(testPlaces.size());
        when(mockPlaceRepository.getPlaces()).thenReturn(testPlaces);

        fakeAsyncChainProvider = new FakeAsyncChainProvider();
        presenter = new ListPresenter(fakeAsyncChainProvider, mockPlaceRepository);
    }

    @Test
    public void attachView_loadsAndDisplaysModels() {
        getPresenter().attachView(getMockView());

        verify(mockPlaceRepository).getPlaces();
        verifyNoMoreInteractions(mockPlaceRepository);

        verify(getMockView()).displayPlacesInList(uiModelListCaptor.capture());
        verifyNoMoreInteractions(getMockView());
        final List<ListedPlaceModel> result = uiModelListCaptor.getValue();
        assertUiModelListsEquals(expectedUiModels, result);
    }

    @Test
    public void reattachView_withLoadedModels_displaysWithoutReloading() {
        attachView_loadsAndDisplaysModels();

        final ListView newMockView = mock(getViewClass());
        getPresenter().reattachView(newMockView);
        verifyNoMoreInteractions(mockPlaceRepository);

        verify(newMockView).displayPlacesInList(uiModelListCaptor.capture());
        verifyNoMoreInteractions(getMockView());
        verifyNoMoreInteractions(newMockView);
        final List<ListedPlaceModel> result = uiModelListCaptor.getValue();
        assertUiModelListsEquals(expectedUiModels, result);
    }

    @Test
    public void reattachView_withoutLoadedModels_loadsAndDisplaysModels() {
        final ListView newMockView = mock(getViewClass());
        getPresenter().reattachView(newMockView);

        verify(mockPlaceRepository).getPlaces();
        verifyNoMoreInteractions(mockPlaceRepository);

        verify(newMockView).displayPlacesInList(uiModelListCaptor.capture());
        verifyNoMoreInteractions(newMockView);
        final List<ListedPlaceModel> result = uiModelListCaptor.getValue();
        assertUiModelListsEquals(expectedUiModels, result);
    }

    @Test
    public void detachView_cancelsSubscriptions() {
        getPresenter().attachView(getMockView());
        verify(getMockView()).displayPlacesInList(anyList());

        getPresenter().detachView();

        for (FakeAsyncChain fakeAsyncChain : fakeAsyncChainProvider.getProvidedAsyncChains()) {
            List<AsyncSubscription> mockSubscriptions = fakeAsyncChain.getMockSubscriptions();
            for (AsyncSubscription mockSubscription : mockSubscriptions) {
                verify(mockSubscription).cancel();
            }
        }
    }

    @Test
    public void showMap_passesExpectedMappedPlaceModelToView() {
        getPresenter().attachView(getMockView());
        verify(getMockView()).displayPlacesInList(anyList());

        final int selectedUiModelPosition = 45;
        final int selectedPlacePosition = testPlaces.size() - (selectedUiModelPosition + 1);
        final Place selectedPlace = testPlaces.get(selectedPlacePosition);

        final MappedPlaceModel expectedMappedModel = new MappedPlaceModel(selectedPlace.latitude,
                selectedPlace.longitude);

        getPresenter().showMap(expectedUiModels.get(selectedUiModelPosition));

        verify(getMockView()).displayPlaceOnMap(mappedPlaceModelCaptor.capture());
        verifyNoMoreInteractions(getMockView());

        final MappedPlaceModel actual = mappedPlaceModelCaptor.getValue();
        assertEquals(expectedMappedModel.latitude, actual.latitude, 0.000_000_1);
        assertEquals(expectedMappedModel.longitude, actual.longitude, 0.000_000_1);
    }

    @Test
    public void applyFilter_filtersExpectedList() {
        getPresenter().attachView(getMockView());
        verify(getMockView()).displayPlacesInList(anyList());

        final String ineffectiveFilter = "Name ";
        final String effectiveFilter = "Name d";

        getPresenter().applyFilter(effectiveFilter);
        verify(getMockView(), times(2)).displayPlacesInList(uiModelListCaptor.capture());
        verifyNoMoreInteractions(getMockView());

        final List<ListedPlaceModel> effectiveFilterResult = uiModelListCaptor.getValue();
        assertEquals(2, effectiveFilterResult.size());
        final ListedPlaceModel leftoverModel = effectiveFilterResult.get(1);
        assertNotNull(leftoverModel);
        assertTrue(leftoverModel.fullName.startsWith(effectiveFilter));

        getPresenter().applyFilter(ineffectiveFilter);
        verify(getMockView(), times(3)).displayPlacesInList(uiModelListCaptor.capture());
        verifyNoMoreInteractions(getMockView());

        final List<ListedPlaceModel> inneffectiveFilterResult = uiModelListCaptor.getValue();
        assertUiModelListsEquals(expectedUiModels, inneffectiveFilterResult);
    }

    private static List<Place> createPlaceList(int count) {
        final List<Place> outputModels = new ArrayList<>(count);
        for (int i = 0; i < count; ++i) {
            // Number these backwards, to ensure the presenter sorts them alphabetically
            int id = count - (i + 1);
            outputModels.add(new Place(id, createExpectedName(id), createExpectedCountry(id),
                    (double) id, (double) id));
        }
        return outputModels;
    }

    private static List<ListedPlaceModel> createExpectedUiModelList(int count) {
        final List<ListedPlaceModel> uiModels = new ArrayList<>(count);
        for (int i = 0; i < count; ++i) {
            uiModels.add(new ListedPlaceModel(i, createExpectedName(i) + ", " + createExpectedCountry(i)));
        }
        return uiModels;
    }

    private static String createExpectedName(int position) {
        return String.format(Locale.US, TEST_NAME, (char) position);
    }

    private static String createExpectedCountry(int position) {
        return String.format(Locale.US, TEST_COUNTRY, position);
    }

    private static void assertUiModelListsEquals(List<ListedPlaceModel> expected, List<ListedPlaceModel> actual) {
        assertEquals(expected.size(), actual.size());
        for (int i = 0; i < expected.size(); ++i) {
            final ListedPlaceModel expectedModel = expected.get(i);
            final ListedPlaceModel actualModel = actual.get(i);

            assertEquals(expectedModel.id, actualModel.id);
            assertEquals(expectedModel.fullName, actualModel.fullName);
        }
    }
}
